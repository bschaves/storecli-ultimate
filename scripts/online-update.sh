#!/usr/bin/env bash
#
#
# Script para baixar e instalar a ultima versão do storecli-ultimate no gitlab.
#
# Atualizado em 2023-08-26

clear

storecli_zip_file=$(mktemp -u)
extract_dir=$(mktemp -d)
stroecli_url_zip='https://gitlab.com/bschaves/storecli-ultimate/-/archive/main/storecli-ultimate-main.zip'


# Necessário ter o wget instalado.
if [[ ! -x $(command -v wget) ]]; then
	echo "ERRO .. instale o wget para prosseguir."
	exit 1
fi

function clean_cache()
{
	echo "Limpando o cache"
	rm -rf "$storecli_zip_file"
	rm -rf "$extract_dir"
}

echo -e "Baixando ... $stroecli_url_zip"
wget -q -O "$storecli_zip_file" "$stroecli_url_zip" || exit 1
unzip "$storecli_zip_file" -d "$extract_dir" 1> /dev/null
cd "$extract_dir"
mv $(ls -d storecli*) storecli
cd storecli
chmod +x setup.sh

./setup.sh install

clean_cache