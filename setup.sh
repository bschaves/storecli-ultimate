#!/usr/bin/env bash
#
#
# Este script automatiza a instalação do script storecli em sistemas linux.
#
#
# Repositório:
# https://gitlab.com/bschaves/storecli-ultimate
# 
# Pacote zip para instalação manual.
# https://gitlab.com/bschaves/storecli-ultimate/-/archive/main/storecli-ultimate-main.zip
#
# Script para automatizar a instalação.
# bash -c 
#
# OBS: seu computador precisa estar conectado a internet para executar este instalador.
# 

__version__='2023-08-26'
__script__=$(readlink -f "$0")
dir_of_project=$(dirname "$__script__")


if [[ $(id -u) == 0 ]]; then
	echo -e "ERRO ... o usuário não pode ser o root."
	exit 1
fi


usage()
{
cat <<EOF
	Use:

	    $(basename $__script__) install     Para instalação offline - se o código fonte completo
	                                        Existir no diretório atual.

	    $(basename $__script__) uninstall   Para desinstalar o script storecli.

EOF
}

# URLs
clienteDownloader=''
URL_ARCHIVE='https://gitlab.com/bschaves/storecli-ultimate/-/archive'
URL_TARFILE_MASTER="$URL_ARCHIVE/main/storecli-ultimate-main.tar.gz"
URL_SRC="$URL_TARFILE_MASTER"

# Diretórios
TEMPORARY_DIR=$(mktemp --directory)
TEMPORARY_FILE=$(mktemp -u)
DIR_UNPACK="$TEMPORARY_DIR/unpack"
DIR_DOWNLOAD="$TEMPORARY_DIR/download"
PREFIX_DIR=~/.local/opt
DIR_BIN=~/.local/bin
DIR_APPLICATIONS=~/.local/share/applications
INSTALATION_DIR="$PREFIX_DIR/storecli-x86_64"
DESTINATION_LINK="$DIR_BIN/storecli"



create_dirs()
{
	mkdir -p "$DIR_UNPACK"
	mkdir -p "$DIR_DOWNLOAD"
	mkdir -p "$DIR_BIN"
	mkdir -p "$PREFIX_DIR"
	mkdir -p "$INSTALATION_DIR"
	mkdir -p "$DIR_APPLICATIONS"
}


clean_cache()
{
	rm -rf $TEMPORARY_DIR 2> /dev/null
	rm -rf $TEMPORARY_FILE 2> /dev/null
}


is_executable()
{
	command -v "$@" >/dev/null 2>&1
}


_ping()
{
	if ! ping -c 1 8.8.8.8 1> /dev/null 2>&1; then
		echo "Verifique sua conexão com a internet."
		sleep 1
		return 1
	fi
	return 0
}

set_client_downloader()
{
	# Verificar gerenciador de downloads do sistema.
	if is_executable aria2c; then
		clienteDownloader='aria2c'
	elif is_executable wget; then
		clienteDownloader='wget'
	elif is_executable curl; then
		clienteDownloader='curl'
	else
		printf "ERRO: Instale uma ferramenta para gerenciar downloads curl|aria2c|wget\n"
		sleep 1
		return 1
	fi
}


download()
{
	set_client_downloader || return 1
	_ping || return 1
	if [ -z $2 ]; then
		echo '(download) ... parâmetro incorreto detectado.'
		return 1
	fi 

	url="$1"
	output_file="$2"

	printf "Conectando ... $url "
	case "$clienteDownloader" in
		aria2c) aria2c "$url" -d $(dirname "$output_file") -o $(basename "$output_file") 1> /dev/null;;
		wget) wget -q "$url" -O "$output_file";;
		curl) curl -fsSL "$url" -o "$output_file";;
		*) echo "ERRO ... nenhum clienteDownloader instalado."; return 1;;
	esac


	if [ $? -eq 0 ]; then
		printf "OK\n"
	else
		printf "ERRO\n"
		return 1
	fi
	return 0
}

_copy_files()
{
	echo -ne "Instalando ... $1 => $2 "
	if cp -R -u "$1" "$2" 1> /dev/null; then
		printf "OK\n"
		return 0
	else
		echo "ERRO (_copy_files) ... $1"
		sleep 1
		return 1
	fi
}


offline_installer()
{
	cd "$dir_of_project"
	[ ! -d ./lib ] && echo "ERRO (offline_installer) ... diretório ./lib não encontrado" && return 1
	[ ! -d ./scripts ] && echo "ERRO (offline_installer) ... diretório ./scripts não encontrado" && return 1
	[ ! -f ./storecli.sh ] && echo "ERRO (offline_installer) ... arquivo ./storecli.sh não encontrado" && return 1
	[ ! -f ./setup.sh ] && echo "ERRO (offline_installer) ... arquivo ./setup.sh não encontrado" && return 1

	create_dirs

	_copy_files lib $INSTALATION_DIR
	_copy_files data $INSTALATION_DIR
	_copy_files scripts $INSTALATION_DIR
	_copy_files bash_libs "$INSTALATION_DIR"
	_copy_files data/storecli.desktop "$DIR_APPLICATIONS"
	_copy_files setup.sh $INSTALATION_DIR
	_copy_files storecli.sh $INSTALATION_DIR
	

	chmod -R a+x $INSTALATION_DIR
	chmod +x "$DIR_APPLICATIONS/storecli.desktop"
	ln -sf $INSTALATION_DIR/storecli.sh $DESTINATION_LINK

	if [ -x $DESTINATION_LINK ]; then
		printf "storecli instalado com sucesso.\n"
		return 0
	else
		printf "ERRO (offline_installer): Falha na instalação, tente novamente\n"
		return 1
	fi
}



_install_external_modules()
{
	_copy_files "bash_libs" "$INSTALATION_DIR"
}


main()
{
	
	if [ -z $1 ]; then
		usage
		clean_cache
		return 0
	fi

	clear
	create_dirs

	case "$1" in 
		-h|--help) usage; return 0;;
		install) 
				offline_installer || return 1
				#_install_external_modules || return 1 
				;;

		uninstall) 
				printf "Desinstalando storecli ... "
				rm -rf $INSTALATION_DIR
				rm -rf $DESTINATION_LINK
				printf "OK\n"
				;;
	esac			
	clean_cache
}



main "$@" || exit 1
exit 0